var gulp = require("gulp");
var concat = require("gulp-concat");
var minify = require("gulp-minify");
var sass = require("gulp-sass")(require("sass"));
var cleanCss = require("gulp-clean-css");
var imagemin = require("gulp-imagemin");
var del = require("del");

gulp.task("pack-js", function () {
  return gulp
    .src(["assets/js/*.js",
    "node_modules/bootstrap/dist/js/bootstrap.js",
    "node_modules/aos/dist/aos.js"
])
    .pipe(concat("script.js"))
    .pipe(minify())
    .pipe(gulp.dest("public/build/js"));
});

gulp.task("pack-css", function () {
  return gulp
    .src([
      "node_modules/bootstrap/dist/css/bootstrap.min.css",
      "node_modules/animate.css/animate.css",
      "node_modules/aos/dist/aos.css",
      "assets/css/*.css",
    ])
    .pipe(concat("style.css"))
    .pipe(cleanCss())
    .pipe(gulp.dest("public/build/css"));
});

gulp.task("scss", function () {
  return gulp
    .src("assets/scss/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("assets/css"));
});

gulp.task("pack-img", function () {
  return gulp
    .src("assets/images/**")
    .pipe(imagemin())
    .pipe(gulp.dest("public/build/images"));
});

gulp.task("clean-js", function () {
  return del(["public/build/js/*.js"]);
});

gulp.task("clean-css", function () {
  return del(["public/build/css/*.css"]);
});

gulp.task("clean-images", function () {
  return del(["public/build/images/*"]);
});

gulp.task("watch", function () {
  gulp.watch("assets/js/**/*.js", gulp.series("clean-js", "pack-js"));
  gulp.watch(
    "assets/scss/**/*.scss",
    gulp.series("clean-css", "scss", "pack-css")
  );
  gulp.watch("assets/images/**/*", gulp.series("clean-images", "pack-img"));
});

gulp.task("default", gulp.series("watch"));
